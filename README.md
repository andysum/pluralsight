# PluralSight Exercise Files #

Visual Studio projects containing source code used as exercise files within PluralSight. They have been modified from the original code so that they can be used within Visual Studio 2015, using MVC, MSTest and Moq frameworks.
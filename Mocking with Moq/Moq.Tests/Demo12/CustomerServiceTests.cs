﻿#region using

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PluralSight.Moq.Code.Demo12;

#endregion

namespace PluralSight.Moq.Tests.Demo12
{
    // Demo: Stubbing Properties
    public class CustomerServiceTests
    {
        [TestClass]
        public class When_creating_a_customer
        {
            [TestMethod]
            public void the_workstation_id_should_be_retrieved()
            {
                //Arrange
                var mockCustomerRepository = new Mock<ICustomerRepository>();
                var mockApplicationSettings = new Mock<IApplicationSettings>();

                // Setting up properties one by one
                //mockApplicationSettings.SetupProperty(x => x.WorkstationId);
                //mockApplicationSettings.SetupProperty(x => x.RevisionNumber);

                mockApplicationSettings.SetupAllProperties();
                mockApplicationSettings.Object.WorkstationId = 23456;
                mockApplicationSettings.Object.RevisionNumber = "ABC";

                var customerService = new CustomerService(
                    mockCustomerRepository.Object, 
                    mockApplicationSettings.Object);

                //Act
                customerService.Create(new CustomerToCreateDto());

                //Assert
                mockApplicationSettings.VerifyGet(x => x.WorkstationId);
                mockApplicationSettings.VerifyGet(x => x.RevisionNumber);
            }
        }
    }
}